import React, { useState } from 'react';
import { SafeAreaProvider } from "react-native-safe-area-context";
import { NavigationContainer } from '@react-navigation/native';

import { initializeApp } from "firebase/app";

import { AuthenticationNavigator } from './src/Authentication';

import { AppNavigator } from './src/App';

import UserContext from './src/components/context/UserContext';

export default function App() {
	const [islogged, setIsLogged] = useState(false)
	const [username, setUsername] = useState("")
	const [email, setEmail] = useState("")
	const [photoURL, setPhotoURL] = useState("")

	const changeIsLoggedStatus = () => {
		setIsLogged(!islogged);
	}

	const changeUsername = (val) => {
		setUsername(val);
	}

	const changeEmail = (val) => {
		setEmail(val);
	}

	const changePhotoURL = (val) => {
		setPhotoURL(val);
	}

	const firebaseConfig = {
		apiKey: "AIzaSyBBOUMa1netxC65zeZzb-p0URzpdSMeRcE",
		authDomain: "findmovies-af627.firebaseapp.com",
		databaseURL: "https://findmovies-af627-default-rtdb.europe-west1.firebasedatabase.app",
		projectId: "findmovies-af627",
		storageBucket: "findmovies-af627.appspot.com",
		messagingSenderId: "435986252136",
		appId: "1:435986252136:web:70afe925f583744aef16dd"
	};

	initializeApp(firebaseConfig);

	return (
		<NavigationContainer>
			<SafeAreaProvider>
				<UserContext.Provider value={{ islogged, changeIsLoggedStatus, username, changeUsername, email, changeEmail, photoURL, changePhotoURL }}>
					{!islogged ? <AuthenticationNavigator /> : <AppNavigator />}
				</UserContext.Provider>
			</SafeAreaProvider>
		</NavigationContainer>
	);
}