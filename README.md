# FindMovies

A React Native application to find the newest movies.

If you want to test the project, click on the following link and scan the QR code to run the project with Expo Go App on your device
Link Project : https://expo.dev/@francoislagadec/find-movies

## Build the project

To build the project, after cloning this repo, you need to install the dependencies with : npm install --legacy-peer-deps

You need to add the --legacy-peer-deps option cause an error occurred with version 16 and 17 of React.

Once it's done, launch the project with : expo start

This will open a web page on which you can run the project on an android emulator/device or on an IOS emulator.
You can also use a QR code to run the project with Expo Go App on your device. 

## Deploy the project
If you want to deploy the project on Expo, you can use : expo --publish, or click on Publish or republish project on web page when expo is started. This will deploy the project on an Expo server with a build of an Android and an IOS version.

Don't forget to be logged into a Expo account to do that.