import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export const COLORS = {
	primary: "#f53b3b",
	white: "#fff",
	black: "#000000",
	blue: "#4096FE",
	gray: "#464646",
	gray1: "#363636",
	mediumGray: "#B3ADAC",
	lightGray: "#dedede",
	transparentWhite: 'rgba(255, 255, 255, 0.2)',
	transparentBlack: 'rgba(0, 0, 0, 0.4)',
};

export const SIZES = {
	base: 8,
	font: 14,
	radius: 12,
	padding: 24,
	largeTitle: 40,
	h1: 30,
	h2: 22,
	h3: 16,
	h4: 14,
	body1: 30,
	body2: 22,
	body3: 16,
	body4: 14,
	body5: 12,
	width,
	height
};