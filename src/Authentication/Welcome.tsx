import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { Routes, StackNavigationProps } from '../components/Navigation';

import { COLORS, SIZES } from '../../constants/theme';

export const SLIDE_HEIGHT = 0.65 * SIZES.height;
const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: COLORS.black
	},
	picture: {
		width: 200,
		height: 200
	},
	titleContainer: {
		paddingVertical: 10,
		paddingHorizontal: 40,
		justifyContent: 'center',
	},
	title: {
		fontSize: 30,
		color: COLORS.white,
		textAlign: 'center'
	},
	descriptionContainer: {
		paddingHorizontal: 60,
		justifyContent: 'center',
	},
	description: {
		fontSize: 18,
		color: COLORS.mediumGray,
		textAlign: 'center'
	},
	buttonContainer: {
		paddingTop: 70,
		paddingHorizontal: 40,
		width: '90%'
	},
	button: {
		marginBottom: 20,
		padding: 10,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: COLORS.primary,
		borderRadius: 5
	},
	buttonText: {
		color: COLORS.white,
		fontSize: 20,
	},
});

const Welcome = ({
	navigation
}: StackNavigationProps<Routes, "Welcome">) => {
	return (
		<View style={styles.container}>
			<Image source={require("../../assets/logo_name_transparency.png")} style={styles.picture} />
			<View style={styles.titleContainer}>
				<Text style={styles.title}>Welcome back</Text>
			</View>
			<View style={styles.descriptionContainer}>
				<Text style={styles.description}>Sign In to your account bellow or Sign Up to enjoy a wonderful experience.</Text>
			</View>
			<View style={styles.buttonContainer}>
				<TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Login')}>
					<Text style={styles.buttonText}>Sign In</Text>
				</TouchableOpacity>
				<TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Register')}>
					<Text style={styles.buttonText}>Sign Up</Text>
				</TouchableOpacity>
			</View>
		</View>
	)
}

export default Welcome;