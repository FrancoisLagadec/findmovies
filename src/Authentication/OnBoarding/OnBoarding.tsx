import React, { useState } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Animated, StatusBar, Platform } from 'react-native';

import Slide from "./Slide";
import Paginator from "./Paginator";
import { Routes, StackNavigationProps } from '../../components/Navigation';

import { COLORS, SIZES } from '../../../constants/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	slider: {
		height: 0.87 * SIZES.height,
		backgroundColor: "black"
	},
	footer: {
		flex: 1
	},
	button: {
		padding: 10,
		width: "90%",
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: COLORS.primary,
		borderRadius: 5
	},
	pagination: {
		//        ...StyleSheet.absoluteFillObject,
	}
});

const slides = [
	{
		title: "Stay informed",
		description: "Check out the list of new movies currently broadcast on theaters each week.",
		picture: require("../../../assets/wallpaper.jpg"),
		mode: 'cover'
	},
	{
		title: "Customize your profile",
		description: "a banch of tools allows you to create a profile that reflects you",
		picture: require("../../../assets/wallpaper.jpg"),
		mode: 'cover'
	},
	{
		title: "Manage your own lists",
		description: "You have the ability to create your own Tops/Lists of movies or series.",
		picture: require("../../../assets/onBoarding3.png"),
		mode: 'contain'
	}
]

const OnBoarding = ({
	navigation,
}: StackNavigationProps<Routes, "OnBoarding">) => {
	const [currentSlideIndex, setCurrentSlideIndex] = useState(0);

	const updateCurrentSlideIndex = e => {
		const contentOffsetX = e.nativeEvent.contentOffset.x;
		const currentIndex = Math.round(contentOffsetX / SIZES.width);
		setCurrentSlideIndex(currentIndex)
	}

	return (
		<View style={styles.container}>
			{
				Platform.OS === "android" ?
					<StatusBar
						translucent
						backgroundColor={COLORS.black}
					/>
					: <></>
			}
			<Animated.View style={styles.slider}>
				<Animated.ScrollView
					onMomentumScrollEnd={updateCurrentSlideIndex}
					horizontal
					snapToInterval={SIZES.width}
					decelerationRate="normal"
					showsHorizontalScrollIndicator={false}
					bounces={false}
					scrollEventThrottle={1}
				>
					{slides.map(({ title, description, picture, mode }, index) => (
						<Slide key={index} {...{ title, description, picture, mode }} />
					))}
				</Animated.ScrollView>
			</Animated.View>
			<View style={styles.footer}>
				<View style={{ ...StyleSheet.absoluteFillObject, backgroundColor: "black", alignItems: 'center', justifyContent: 'center' }}>
					<View style={styles.pagination}>
						<Paginator data={slides} currentIndex={currentSlideIndex} />
					</View>
					<TouchableOpacity
						style={styles.button}
						onPress={() => { navigation.navigate("Welcome") }}
					>
						<Text style={{ color: "white", fontSize: 18 }}>BEGIN</Text>
					</TouchableOpacity>
				</View>
			</View>
		</View>
	)
}

export default OnBoarding;