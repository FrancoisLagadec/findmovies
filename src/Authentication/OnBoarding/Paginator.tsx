import React from 'react';
import { View, StyleSheet } from 'react-native';

import { COLORS } from '../../../constants/theme';

const styles = StyleSheet.create({
	dot: {
		height: 10,
		borderRadius: 5,
		marginHorizontal: 8,
		width: 10
	}
});

interface PaginatorProps {
	data: any;
	currentIndex: number;
}

const Paginator = ({ data, currentIndex }: PaginatorProps) => {
	return (
		<View style={{ flexDirection: 'row', height: 24 }}>
			{data.map((_, index) => {
				return <View style={[styles.dot, currentIndex === index ? { backgroundColor: COLORS.primary } : { backgroundColor: COLORS.mediumGray }]} key={index.toString()} />
			})}
		</View>
	)
}

export default Paginator;
