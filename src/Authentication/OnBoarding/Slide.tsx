import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';

import { COLORS, SIZES } from '../../../constants/theme';

export const SLIDE_HEIGHT = 0.65 * SIZES.height;

const styles = StyleSheet.create({
	container: {
		width: SIZES.width
	},
	titleContainer: {
		height: 80,
		paddingHorizontal: 40,
		justifyContent: 'center',
		transform: [{ translateY: (SLIDE_HEIGHT - 80 / 2) }]

	},
	underlay: {
		...StyleSheet.absoluteFillObject,
		justifyContent: 'flex-end',
	},
	picture: {
		...StyleSheet.absoluteFillObject,
		width: undefined,
		height: undefined
	},
	title: {
		fontSize: 30,
		color: COLORS.white,
		textAlign: 'center'
	},
	descriptionContainer: {
		paddingHorizontal: 60,
		height: 100,
		justifyContent: 'center',
		transform: [{ translateY: (SLIDE_HEIGHT - 80 / 2) }]
	},
	description: {
		fontSize: 18,
		color: COLORS.mediumGray,
		textAlign: 'center'
	}
});

interface SlideProps {
	title: string;
	description: string;
	picture: number;
	mode: string;
}

const Slide = ({ title, description, picture, mode }: SlideProps) => {
	return (
		<View style={styles.container}>
			<View style={styles.underlay}>
				<ImageBackground source={picture} style={styles.picture} resizeMode={mode}>
					<LinearGradient
						locations={[0.1, 0.3, 0.4, 0.85]}
						colors={[
							'rgba(0, 0, 0, 0.5)',
							'rgba(0, 0, 0, 0.2)',
							'rgba(0, 0, 0, 0.2)',
							'rgba(0, 0, 0, 1)',
						]}
						style={{ height: "100%" }}>

					</LinearGradient>
				</ImageBackground>
			</View>
			<View style={styles.titleContainer}>
				<Text style={styles.title}>{title}</Text>
			</View>
			<View style={styles.descriptionContainer}>
				<Text style={styles.description}>{description}</Text>
			</View>
		</View>
	)
}

export default Slide;
