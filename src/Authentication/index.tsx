import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { Routes } from "../components/Navigation";

import OnBoarding from "./OnBoarding";
import Welcome from "./Welcome";
import Login from "./Login";
import Register from "./Register";
import ForgotPwd from "./ForgotPwd";
import PasswordChanged from "./PasswordChanged";

const AuthenticationStack = createStackNavigator<Routes>();
export const AuthenticationNavigator = () => {
	return (
		<AuthenticationStack.Navigator screenOptions={{ headerShown: false }}>
			<AuthenticationStack.Screen name="OnBoarding" component={OnBoarding} />
			<AuthenticationStack.Screen name="Welcome" component={Welcome} />
			<AuthenticationStack.Screen name="Login" component={Login} />
			<AuthenticationStack.Screen name="Register" component={Register} />
			<AuthenticationStack.Screen name="ForgotPwd" component={ForgotPwd} />
			<AuthenticationStack.Screen name="PasswordChanged" component={PasswordChanged} />
		</AuthenticationStack.Navigator>
	);
};