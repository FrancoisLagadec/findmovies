import React, { useContext } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Feather } from '@expo/vector-icons';
import { getAuth, createUserWithEmailAndPassword, updateProfile } from "firebase/auth";

import InputWithLogo from '../components/Form/InputWithLogo';
import InputPassword from '../components/Form/InputPassword';

import UserContext from '../components/context/UserContext';

import { Routes, StackNavigationProps } from '../components/Navigation';

import { COLORS, SIZES } from '../../constants/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: COLORS.black
	},
	picture: {
		width: 200,
		height: 200
	},
	buttonContainer: {
		paddingTop: 30,
		width: '90%',
		alignSelf: 'center',
	},
	button: {
		marginBottom: 20,
		padding: 10,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: COLORS.primary,
		borderRadius: 5
	},
	buttonText: {
		color: COLORS.white,
		fontSize: 20,
	},
});

const RegisterSchema = Yup.object().shape({
	username: Yup.string()
		.min(2, 'Too short !')
		.max(50, 'Too long !')
		.required('Required'),
	email: Yup.string().email('Invalid email address').required('Required'),
	password: Yup.string()
		.min(8, 'Password must be at least 8 characters')
		.max(50, 'Password must not exceed 50 characters')
		.required('Required'),
	passwordConfirm: Yup.string()
		.required('Required')
		.oneOf([Yup.ref('password')], "Those passwords didn't match. Try again."),
});

const Register = ({
	navigation
}: StackNavigationProps<Routes, "Register">) => {
	const userContext = useContext(UserContext);
	const auth = getAuth();

	return (
		<ScrollView style={styles.container}>

			<TouchableOpacity
				style={{
					alignItems: 'center',
					justifyContent: 'center',
					width: 40,
					height: 40,
					marginTop: SIZES.padding,
					left: 15,
					borderRadius: 15,
					backgroundColor: COLORS.transparentBlack
				}}
				onPress={() => navigation.goBack()}
			>
				<Feather
					name={"chevron-left"}
					size={30}
					color={COLORS.white}
				/>
			</TouchableOpacity>

			<View style={{
				alignItems: 'center',
				justifyContent: 'center',
			}}>
				<Image source={require("../../assets/logo_name_transparency.png")} style={styles.picture} />
				<Formik
					validationSchema={RegisterSchema}
					initialValues={{ username: "", email: "", password: "", passwordConfirm: "" }}
					onSubmit={values => {
						createUserWithEmailAndPassword(auth, values.email, values.password)
							.then(() => {
								updateProfile(auth.currentUser, {
									displayName: values.username,
									photoURL: 'https://www.component-creator.com/images/testimonials/defaultuser.png'
								}).then(() => {
									userContext.changeUsername(auth.currentUser.displayName);
									userContext.changeEmail(auth.currentUser.email);
									userContext.changePhotoURL(auth.currentUser.photoURL);
									userContext.changeIsLoggedStatus();
								}).catch((error) => {
									const errorCode = error.code;
									const errorMessage = error.message;

									console.error(errorCode, errorMessage);
								})
							})
							.catch((error) => {
								const errorCode = error.code;
								const errorMessage = error.message;

								console.error(errorCode, errorMessage);
							})
					}}
				>
					{({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
						<View>
							<InputWithLogo
								logo={"user"}
								height={20}
								label={"Enter your username..."}
								value={values.username}
								onChange={handleChange("username")}
								onBlur={handleBlur('username')}
								error={errors.username}
								touched={touched.username}
							/>
							<InputWithLogo
								logo={"mail"}
								height={20}
								label={"Enter your email address..."}
								value={values.email}
								onChange={handleChange("email")}
								onBlur={handleBlur('email')}
								error={errors.email}
								touched={touched.email}
							/>
							<InputPassword
								height={20}
								label={"Enter your password..."}
								value={values.password}
								onChange={handleChange("password")}
								onBlur={handleBlur('password')}
								error={errors.password}
								touched={touched.password}
							/>
							<InputPassword
								height={20}
								label={"Confirm your password..."}
								value={values.passwordConfirm}
								onChange={handleChange("passwordConfirm")}
								onBlur={handleBlur('passwordConfirm')}
								error={errors.passwordConfirm}
								touched={touched.passwordConfirm}
							/>
							<View style={styles.buttonContainer}>
								<TouchableOpacity style={styles.button} onPress={() => handleSubmit()}>
									<Text style={styles.buttonText}>Create your account</Text>
								</TouchableOpacity>
							</View>
						</View>
					)}
				</Formik>
			</View>
		</ScrollView>
	);
}

export default Register;