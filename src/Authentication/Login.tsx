import React, { useContext, useEffect, useState } from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Feather } from '@expo/vector-icons';
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

import InputWithLogo from '../components/Form/InputWithLogo';
import InputPassword from '../components/Form/InputPassword';
import CheckBox from '../components/Form/CheckBox';

import UserContext from '../components/context/UserContext';

import { Routes, StackNavigationProps } from '../components/Navigation';

import { COLORS, SIZES } from '../../constants/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: COLORS.black
	},
	picture: {
		width: 200,
		height: 200
	},
	buttonContainer: {
		paddingTop: 30,
		paddingBottom: 30,
		width: '90%',
		justifyContent: 'center',
		alignSelf: 'center',
	},
	button: {
		marginBottom: 20,
		padding: 10,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: COLORS.primary,
		borderRadius: 5
	},
	buttonText: {
		color: COLORS.white,
		fontSize: 20,
	},
	forgotPassword: {
		alignItems: 'center',
		justifyContent: 'center',
	},
	forgotPasswordText: {
		color: COLORS.white,
		fontSize: 16,
	},
});

const LoginSchema = Yup.object().shape({
	email: Yup.string().email('Invalid email address').required('Required'),
	password: Yup.string()
		.min(8, 'Password must be at least 8 characters')
		.max(50, 'Password must not exceed 50 characters')
		.required('Required'),
});

const Login = ({
	navigation
}: StackNavigationProps<Routes, "Login">) => {
	const [initialValueCheckBox, setInitialValueCheckBox] = useState(false)
	const [initialValueEmail, setInitialValueEmail] = useState("yyy")

	const userContext = useContext(UserContext);
	const auth = getAuth();

	useEffect(() => {
		CheckToogle()
	}, [])

	const CheckToogle = async () => {
		return await AsyncStorage
			.getItem('FindMovies-RM')
			.then((result) => result)
	}

	async function getRememberedUser() {
		const checkBox = await AsyncStorage.getItem('FindMovies-RM')
		if (checkBox !== null && JSON.parse(checkBox) === true) {
			const user = await AsyncStorage.getItem('FindMovies-RM-email')
			setInitialValueEmail(JSON.stringify(user))
		}
	}

	return (
		<ScrollView style={styles.container}>
			<TouchableOpacity
				style={{
					alignItems: 'center',
					justifyContent: 'center',
					width: 40,
					height: 40,
					marginTop: SIZES.padding,
					left: 15,
					borderRadius: 15,
					backgroundColor: COLORS.transparentBlack
				}}
				onPress={() => navigation.goBack()}
			>
				<Feather
					name={"chevron-left"}
					size={30}
					color={COLORS.white}
				/>
			</TouchableOpacity>

			<View style={{ alignItems: 'center', justifyContent: 'center' }}>
				<Image source={require("../../assets/logo_name_transparency.png")} style={styles.picture} />
				<Formik
					validationSchema={LoginSchema}
					initialValues={{ email: "", password: "", remember: false }}
					onSubmit={values => {
						signInWithEmailAndPassword(auth, values.email, values.password)
							.then(async (val) => {
								if (values.remember) {
									await AsyncStorage.multiSet([['FindMovies-RM', JSON.stringify(true)], ['FindMovies-RM-email', values.email]])
								}
								userContext.changePhotoURL(auth.currentUser.photoURL)
								userContext.changeUsername(val.user.displayName);
								userContext.changeEmail(val.user.email);
								userContext.changeIsLoggedStatus();
							})
							.catch((error) => {
								const errorCode = error.code;
								const errorMessage = error.message;

								console.error(errorCode, errorMessage);
							})
					}}
				>
					{({ handleChange, handleBlur, setFieldValue, handleSubmit, values, errors, touched }) => (
						<View>
							<InputWithLogo
								logo={"mail"}
								height={20}
								label={"Enter your email address..."}
								value={values.email}
								onChange={handleChange("email")}
								onBlur={handleBlur('email')}
								error={errors.email}
								touched={touched.email}
							/>

							<InputPassword
								height={20}
								label={"Enter your password..."}
								value={values.password}
								onChange={handleChange("password")}
								onBlur={handleBlur('password')}
								error={errors.password}
								touched={touched.password}
							/>

							<View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
								<CheckBox
									label="Remember me"
									size={20}
									checked={values.remember}
									onChange={() => setFieldValue("remember", !values.remember)}
								/>
								<TouchableOpacity style={styles.forgotPassword} onPress={() => navigation.navigate("ForgotPwd")}>
									<Text style={styles.forgotPasswordText}>Forgot password ?</Text>
								</TouchableOpacity>
							</View>

							<View style={styles.buttonContainer}>
								<TouchableOpacity
									style={styles.button}
									onPress={() => handleSubmit()}>
									<Text style={styles.buttonText}>Sign In</Text>
								</TouchableOpacity>
							</View>
						</View>
					)}
				</Formik>
			</View>
		</ScrollView>
	);
}

export default Login;
