import React from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Feather } from '@expo/vector-icons';

import InputWithLogo from '../components/Form/InputWithLogo';

import { Routes, StackNavigationProps } from '../components/Navigation';
import { COLORS, SIZES } from '../../constants/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: COLORS.black
	},
	picture: {
		width: 200,
		height: 200
	},
	buttonContainer: {
		paddingTop: 30,
		paddingBottom: 30,
		width: '90%',
		justifyContent: 'center',
		alignSelf: 'center',
	},
	button: {
		marginBottom: 20,
		padding: 10,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: COLORS.primary,
		borderRadius: 5
	},
	buttonText: {
		color: COLORS.white,
		fontSize: 20,
	},
	titleContainer: {
		paddingVertical: 10,
		paddingHorizontal: 40,
		justifyContent: 'center',
	},
	title: {
		fontSize: 30,
		color: COLORS.white,
		textAlign: 'center'
	},
	descriptionContainer: {
		paddingHorizontal: 60,
		justifyContent: 'center',
	},
	description: {
		fontSize: 18,
		color: COLORS.mediumGray,
		textAlign: 'center'
	},
});

const ForgotPwdSchema = Yup.object().shape({
	email: Yup.string().email('Invalid email address').required('Required'),
});

const ForgotPwd = ({
	navigation
}: StackNavigationProps<Routes, "ForgotPwd">) => {
	return (
		<ScrollView style={styles.container}>
			<TouchableOpacity
				style={{
					alignItems: 'center',
					justifyContent: 'center',
					width: 40,
					height: 40,
					marginTop: SIZES.padding,
					left: 15,
					borderRadius: 15,
					backgroundColor: COLORS.transparentBlack
				}}
				onPress={() => navigation.goBack()}
			>
				<Feather
					name={"chevron-left"}
					size={30}
					color={COLORS.white}
				/>
			</TouchableOpacity>

			<View style={{ alignItems: 'center', justifyContent: 'center' }}>
				<Image source={require("../../assets/logo_name_transparency.png")} style={styles.picture} />
				<View style={styles.titleContainer}>
					<Text style={styles.title}>Some problems with your password ?</Text>
				</View>
				<View style={styles.descriptionContainer}>
					<Text style={styles.description}>Please pass on your email address bellow, and we will send you a new password by email.</Text>
				</View>
				<Formik
					validationSchema={ForgotPwdSchema}
					initialValues={{ email: "" }}
					onSubmit={() => navigation.navigate("PasswordChanged")}
				>
					{({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
						<View>
							<InputWithLogo
								logo={"mail"}
								height={20}
								label={"Enter your email address..."}
								value={values.email}
								onChange={handleChange("email")}
								onBlur={handleBlur('email')}
								error={errors.email}
								touched={touched.email}
							/>

							<View style={styles.buttonContainer}>
								<TouchableOpacity
									style={styles.button}
									onPress={() => handleSubmit}>
									<Text style={styles.buttonText}>Send</Text>
								</TouchableOpacity>
							</View>
						</View>
					)}
				</Formik>
			</View>
		</ScrollView>
	);
}

export default ForgotPwd;
