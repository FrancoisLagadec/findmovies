import React from 'react';
import { Image, StyleSheet, Text, View, ScrollView } from 'react-native';
import { COLORS } from '../../constants/theme';

import { Routes, StackNavigationProps } from '../components/Navigation';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: COLORS.black
	},
	picture: {
		width: 200,
		height: 200
	},
	titleContainer: {
		paddingVertical: 10,
		paddingHorizontal: 40,
		justifyContent: 'center',
	},
	title: {
		fontSize: 30,
		color: COLORS.white,
		textAlign: 'center'
	},
	descriptionContainer: {
		paddingHorizontal: 60,
		justifyContent: 'center',
	},
	description: {
		fontSize: 18,
		color: COLORS.mediumGray,
		textAlign: 'center'
	},
});

const PasswordChanged = ({
	navigation
}: StackNavigationProps<Routes, "PasswordChanged">) => {
	return (
		<ScrollView style={styles.container}>
			<View style={{ alignItems: 'center', justifyContent: 'center' }}>
				<Image source={require("../../assets/logo_name_transparency.png")} style={styles.picture} />
				<View style={styles.titleContainer}>
					<Text style={styles.title}>Password successfully changed</Text>
				</View>
				<View style={styles.descriptionContainer}>
					<Text style={styles.description}>Your password have been successfully changed. We have sent you an email with your new password.</Text>
				</View>
			</View>
		</ScrollView>
	);
}

export default PasswordChanged;
