import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { Routes } from "../../components/Navigation";

import HomeScreen from "./HomeScreen";
import MovieDetails from "../MovieDetails";

const HomeStack = createStackNavigator<Routes>();
export const HomeNavigator = () => {
	return (
		<HomeStack.Navigator screenOptions={{ headerShown: false }}>
			<HomeStack.Screen name="HomeScreen" component={HomeScreen} />
			<HomeStack.Screen name="MovieDetails" component={MovieDetails} />
		</HomeStack.Navigator>
	);
};