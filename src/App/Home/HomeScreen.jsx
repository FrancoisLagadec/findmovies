import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, FlatList, TouchableWithoutFeedback, SafeAreaView, StatusBar } from 'react-native';
import * as _boxOffice from "../../../mocks/boxOffice.json";
import * as _trending from "../../../mocks/trending.json";
import { COLORS, SIZES } from '../../../constants/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: COLORS.black,
	},
	subSectionContainer: {
		paddingTop: 15,
		marginBottom: 90
	},
});

export default function HomeScreen({ navigation }) {
	const [boxOffice, setBoxOffice] = useState(null)
	const [trending, setTrending] = useState(null)

	const APIKey = "6f84ec42acmshc5e7e7ad58156e7p1dda4fjsn1dd506edb057"

	function getBoxOffice() {
		return fetch("https://movies-tvshows-data-imdb.p.rapidapi.com/?type=get-boxoffice-movies&page=1", {
			"method": "GET",
			"headers": {
				"x-rapidapi-host": "movies-tvshows-data-imdb.p.rapidapi.com",
				"x-rapidapi-key": APIKey
			}
		})
			.then(res => res.json())
			.then(data => Promise.all(data.movie_results.map(getImage)))
	}

	function getTrending() {
		return fetch("https://movies-tvshows-data-imdb.p.rapidapi.com/?type=get-trending-movies&page=1", {
			"method": "GET",
			"headers": {
				"x-rapidapi-host": "movies-tvshows-data-imdb.p.rapidapi.com",
				"x-rapidapi-key": APIKey
			}
		})
			.then(res => res.json())
			.then(data => Promise.all(data.movie_results.map(getImage)))
	}

	function getImage(data) {
		return fetch(`https://movies-tvshows-data-imdb.p.rapidapi.com/?type=get-movies-images-by-imdb&imdb=${data.imdb_id}`, {
			"method": "GET",
			"headers": {
				"x-rapidapi-host": "movies-tvshows-data-imdb.p.rapidapi.com",
				"x-rapidapi-key": APIKey
			}
		})
			.then(res => res.json())
			.then(info => ({
				imdb_id: data.imdb_id,
				title: data.title,
				year: data.year,
				poster: info.poster,
				fanart: info.fanart
			}));
	}


	useEffect(() => {
		// Run! Like go get some data from an API.

		/*	getBoxOffice()
				.then(setBoxOffice)
				.catch(console.log)
		*/
		setBoxOffice(_boxOffice.movie_results)

		/*	getTrending()
				.then(setTrending)
				.catch(console.log)
		*/
		setTrending(_trending.movie_results)
	}, []);

	function renderMoviesSection(section) {
		return (
			<View
				style={{
					marginTop: SIZES.padding + 6,
				}}
			>

				{/* HEADER */}
				<View
					style={{
						flexDirection: 'row',
						paddingHorizontal: SIZES.padding,
						alignItems: 'center'
					}}
				>
					<Text style={{ color: COLORS.white, fontSize: SIZES.h2, fontWeight: 'bold' }}
					>
						{section}
					</Text>
				</View>

				{/* LIST */}
				<FlatList
					horizontal
					showsVerticalScrollIndicator={false}
					contentContainerStyle={{
						marginTop: SIZES.padding
					}}
					data={section === "In Theaters" ? boxOffice : trending}
					keyExtractor={item => `${item.imdb_id}`}
					renderItem={({ item, index }) => {
						return (
							<TouchableWithoutFeedback
								onPress={() => navigation.navigate('MovieDetails', { selectedMovie: item })}
							>
								<View
									style={{
										marginLeft: index == 0 ? SIZES.padding : 20,
										marginRight: index == boxOffice.length - 1 ? SIZES.padding : 0
									}}
								>

									{/* IMAGE */}
									<Image
										source={{ uri: item.poster }}
										resizeMode="cover"
										style={{
											width: SIZES.width / 3,
											height: (SIZES.width / 3) + 60,
											borderRadius: 20
										}}
									/>

									{/* NAME */}
									<Text
										style={{
											marginTop: SIZES.base,
											color: COLORS.white,
											fontSize: SIZES.h4,
											width: (SIZES.width / 3) - 10

										}}>
										{item.title}
									</Text>

								</View>
							</TouchableWithoutFeedback>
						)
					}}
				/>
			</View>
		)
	}

	return (
		<SafeAreaView style={styles.container}>
			{
				Platform.OS === "android" ?
					<StatusBar
						translucent
						backgroundColor="transparent"
					/>
					: <></>
			}
			<ScrollView contentContainerStyle={{ paddingBottom: 80 }}>

				{renderMoviesSection("In Theaters")}

				{renderMoviesSection("Trending")}

			</ScrollView>
		</SafeAreaView>
	);
}