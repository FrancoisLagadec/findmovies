import React, { useRef } from 'react';
import { View, Animated } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { FontAwesome5 } from '@expo/vector-icons';

import { AnimationBottomBar, getWidth } from '../components/utils/AnimationBottomBar';

import { HomeNavigator } from './Home';
import SearchScreen from './Search/SearchScreen';
import CollectionScreen from './Collection/CollectionScreen';
import { UserNavigator } from './User';

import { COLORS } from '../../constants/theme';

const Tab = createBottomTabNavigator();

export const AppNavigator = () => {
	const tabOffsetValue = useRef(new Animated.Value(0)).current;

	return (<>
		<Tab.Navigator
			screenOptions={{
				tabBarActiveTintColor: COLORS.primary,
				tabBarShowLabel: false,
				headerShown: false,
				tabBarStyle: {
					backgroundColor: COLORS.black,
					position: 'absolute',
					bottom: 0,
					height: 60,
				}
			}}>

			{
				// Tab Screens
			}
			<Tab.Screen name="Home" component={HomeNavigator} options={{
				tabBarIcon: ({ focused }) => (
					<View>
						<FontAwesome5
							name="home"
							size={20}
							color={focused ? COLORS.primary : COLORS.gray}
						>
						</FontAwesome5>
					</View>
				)
			}} listeners={({ navigation, route }) => ({
				// Onpress Update...
				tabPress: e => {
					Animated.spring(tabOffsetValue, {
						toValue: 0,
						useNativeDriver: true
					}).start();
				}
			})} />
			<Tab.Screen name="Search" component={SearchScreen} options={{
				tabBarIcon: ({ focused }) => (
					<View>
						<FontAwesome5
							name="search"
							size={20}
							color={focused ? COLORS.primary : COLORS.gray}
						>
						</FontAwesome5>
					</View>
				)
			}} listeners={({ navigation, route }) => ({
				// Onpress Update...
				tabPress: e => {
					Animated.spring(tabOffsetValue, {
						toValue: getWidth(),
						useNativeDriver: true
					}).start();
				}
			})} />
			<Tab.Screen name="Collection" component={CollectionScreen} options={{
				tabBarIcon: ({ focused }) => (
					<View>
						<FontAwesome5
							name="clipboard-list"
							size={20}
							color={focused ? COLORS.primary : COLORS.gray}
						>
						</FontAwesome5>
					</View>
				)
			}} listeners={({ navigation, route }) => ({
				// Onpress Update...
				tabPress: e => {
					Animated.spring(tabOffsetValue, {
						toValue: getWidth() * 2,
						useNativeDriver: true
					}).start();
				}
			})} />
			<Tab.Screen name="User" component={UserNavigator} options={{
				tabBarIcon: ({ focused }) => (
					<View>
						<FontAwesome5
							name="user-alt"
							size={20}
							color={focused ? COLORS.primary : COLORS.gray}
						>
						</FontAwesome5>
					</View>
				)
			}} listeners={({ navigation, route }) => ({
				// Onpress Update...
				tabPress: e => {
					Animated.spring(tabOffsetValue, {
						toValue: getWidth() * 3,
						useNativeDriver: true
					}).start();
				}
			})} />
		</Tab.Navigator>
		<AnimationBottomBar tabOffsetValue={tabOffsetValue} />
	</>
	)
}
