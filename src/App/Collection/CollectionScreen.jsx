import React, { useRef, useState, useEffect } from 'react';
import { Animated, StyleSheet, Text, View, TouchableOpacity, SafeAreaView, StatusBar, ScrollView } from 'react-native';
import { getAuth } from "firebase/auth";
import { getDatabase, ref } from "firebase/database";
import { Ionicons } from '@expo/vector-icons';

import { getWishes } from '../../components/GetData';
import CardList from '../../components/CardList';

import { COLORS, SIZES } from '../../../constants/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: COLORS.black,
	},
	header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: SIZES.padding + 6,
		paddingHorizontal: SIZES.padding,
	},
	filterTxt: {
		color: COLORS.primary,
		fontWeight: 'bold',
		fontSize: 22,
	},
	filterBtn: {
		right: 0
	},
	tabSelected: {
		alignSelf: 'center',
		textAlign: 'center',
		fontWeight: 'bold',
		fontSize: 18,
		color: COLORS.white,
		backgroundColor: COLORS.primary,
		width: (SIZES.width - 48) / 2,
		paddingVertical: 13,
		borderRadius: 50,
	},
	tabUnselected: {
		alignSelf: 'center',
		fontWeight: 'bold',
		fontSize: 18,
		color: '#A0A0A0'
	}
});

function Tabs({ children }) {
	return (
		<View
			style={{
				width: SIZES.width - 48,
				backgroundColor: '#E5E8E8',
				justifyContent: 'space-around',
				alignItems: 'center',
				flexDirection: 'row',
				marginTop: 20,
				borderRadius: 50,
			}}
		>
			{children}
		</View>
	)
}

export default function CollectionScreen({ navigation }) {
	const [tabSelected, setTabSelected] = useState(0);
	const [wishes, setWishes] = useState([]);
	const tabOffsetValue = useRef(new Animated.Value(0)).current;

	const auth = getAuth();
	const db = getDatabase();
	const dbRef = ref(getDatabase());

	useEffect(() => {
		getWishes(dbRef, auth.currentUser.uid)
			.then((result) => {
				setWishes(result);
			})
	}, [])

	function renderListsSection() {
		return (
			<View
				style={{
					paddingHorizontal: SIZES.padding,
				}}>
				{/* HeaderList */}
				<Tabs>
					<TouchableOpacity
						style={[
							{
								width: SIZES.width / 2,
								height: 50,
								justifyContent: 'center',
								alignItems: 'center',
							},
						]}
						onPress={() => {
							setTabSelected(0)
						}}
					>
						<Text style={!tabSelected ? styles.tabSelected : styles.tabUnselected}>
							Notes
						</Text>
					</TouchableOpacity>
					<TouchableOpacity
						style={[
							{
								width: SIZES.width / 2,
								height: 50,
								justifyContent: 'center',
								alignItems: 'center',
							},
						]}
						onPress={() => {
							setTabSelected(1)
						}}
					>
						<Text style={!tabSelected ? styles.tabUnselected : styles.tabSelected}>
							Wishes
						</Text>
					</TouchableOpacity>
				</Tabs>

				{/* List */}
				<ScrollView
					style={{
						marginBottom: 130
					}}
				>
					{
						!tabSelected ?
							<View
								style={{
									height: SIZES.width,
									alignItems: 'center',
									justifyContent: 'center'
								}}>
								<Text
									style={{
										textAlignVertical: 'auto',
										color: COLORS.lightGray,
										fontSize: SIZES.h3
									}}
								>
									You haven't rated a film yet.
								</Text>
							</View>
							: wishes && wishes.length > 0 ?
								<CardList navigation={navigation} style={{ flex: 1 }} results={wishes} />
								:
								<View
									style={{
										height: SIZES.width,
										alignItems: 'center',
										justifyContent: 'center'
									}}>
									<Text
										style={{
											textAlignVertical: 'auto',
											color: COLORS.lightGray,
											fontSize: SIZES.h3
										}}>
										You have not yet created a list.
									</Text>
								</View>
					}
				</ScrollView>
			</View>
		)
	}

	return (
		<SafeAreaView style={[styles.container, { paddingBottom: 60 }]}>
			{
				Platform.OS === "android" ?
					<StatusBar
						translucent
						backgroundColor="transparent"
					/>
					: <></>
			}

			{/* Header */}
			<View style={styles.header}>
				<Text style={{ color: COLORS.white, fontSize: SIZES.h2, fontWeight: 'bold' }}
				>
					Notes and Wishes
				</Text>
				<TouchableOpacity
					style={styles.filterBtn}
					onPress={() => {
						getWishes(dbRef, auth.currentUser.uid)
							.then((result) => {
								setWishes(result);
							})
					}}
				>
					<Ionicons name="reload" size={24} color={COLORS.primary} />
				</TouchableOpacity>
			</View>

			{renderListsSection()}
		</SafeAreaView>
	);
}