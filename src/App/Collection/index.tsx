import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { Routes } from "../../components/Navigation";

import CollectionScreen from "./CollectionScreen";

const CollectionStack = createStackNavigator<Routes>();
export const CollectionNavigator = () => {
	return (
		<CollectionStack.Navigator screenOptions={{ headerShown: false }}>
			<CollectionStack.Screen name="CollectionScreen" component={CollectionScreen} />
		</CollectionStack.Navigator>
	);
};