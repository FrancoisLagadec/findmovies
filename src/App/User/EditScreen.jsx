import React, { useContext } from 'react';
import { StatusBar } from "expo-status-bar";
import { StyleSheet, SafeAreaView, View, Text, TouchableOpacity } from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Feather } from '@expo/vector-icons';
import { getAuth, updateProfile, updateEmail, EmailAuthProvider, reauthenticateWithCredential } from "firebase/auth";

import InputPassword from '../../components/Form/InputPassword';
import InputWithLogo from '../../components/Form/InputWithLogo';

import UserContext from '../../components/context/UserContext';

import { COLORS, SIZES } from '../../../constants/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: COLORS.black,
	},
	inputContainer: {
		marginTop: StatusBar.currentHeight,
		alignItems: "center",
		justifyContent: "center",
		flex: 1
	},
	view_header: {
		flex: 1,
		width: "100%",
		flexDirection: "row",
		alignItems: "flex-start",
		justifyContent: "center",
	},
	username: {
		fontSize: 24,
		color: COLORS.white,
		fontWeight: "bold",
	},
	description: {
		color: "#535151",
		fontSize: 14,
		lineHeight: 14,
		fontWeight: "500",
	},
	view_RepoLowerPart: {
		paddingHorizontal: SIZES.padding,
		backgroundColor: COLORS.gray,
		flex: 3,
		borderRadius: 15,
		width: "100%",
		marginTop: 20,
	},
	view_RepoTabs: {
		flex: 2,
		backgroundColor: "#FFFFFF",
		justifyContent: "center",
		flexDirection: "row",
	},
	button: {
		padding: 10,
		width: "60%",
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: "#f53b3b",
		borderRadius: 5,
		marginTop: 20
	},
	picture: {
		width: 200,
		height: 200
	},
	buttonContainer: {
		paddingTop: 30,
		width: '90%',
		alignSelf: 'center',
	},
	button: {
		marginBottom: 20,
		padding: 10,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: "#f53b3b",
		borderRadius: 5
	},
	buttonText: {
		color: '#FFFFFF',
		fontSize: 20,
	},
	end_view: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "flex-end",
		paddingRight: 20,
	},
});

export default function EditScreen({ navigation }) {

	const auth = getAuth();

	const userContext = useContext(UserContext);

	const EditSchema = Yup.object().shape({
		currentPassword: Yup.string()
			.min(8, 'Password must be at least 8 characters')
			.max(50, 'Password must not exceed 50 characters')
			.required('Required'),
		username: Yup.string()
			.min(2, 'Too short !')
			.max(50, 'Too long !')
			.required('Required'),
		email: Yup.string().email('Invalid email address').required('Required'),
	});

	return (
		<SafeAreaView style={styles.container}>
			<TouchableOpacity
				style={{
					alignItems: 'center',
					justifyContent: 'center',
					width: 40,
					height: 40,
					marginTop: SIZES.padding,
					left: 15,
					borderRadius: 15,
					backgroundColor: COLORS.transparentBlack
				}}
				onPress={() => navigation.goBack()}
			>
				<Feather
					name={"chevron-left"}
					size={30}
					color={COLORS.white}
				/>
			</TouchableOpacity>
			<View style={styles.inputContainer}>
				<Formik
					validationSchema={EditSchema}
					initialValues={{ currentPassword: "", username: auth.currentUser.displayName, email: auth.currentUser.email }}
					onSubmit={values => {
						reauthenticateWithCredential(
							auth.currentUser,
							EmailAuthProvider.credential(
								auth.currentUser.email,
								values.currentPassword))
							.then(() => {
								updateProfile(auth.currentUser, {
									displayName: values.username,
								}).then(() => {
									userContext.changeUsername(auth.currentUser.displayName);
									userContext.changeEmail(values.email);
									console.log('Profile updated successfully');
									console.log("User infos = ", auth.currentUser)
								}).catch((error) => {
									console.error(error.code, error.message);
								}),
									updateEmail(auth.currentUser, values.email)
										.then(() => {
											console.log('Profile updated successfully');
											console.log("User infos = ", auth.currentUser)
										}).catch((error) => {
											console.error(error.code, error.message);
										}),
									navigation.navigate("UserScreen")
							}).catch((error) => {
								console.error(error.code, error.message);
							})
					}
					}
				>
					{({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
						<View>
							<InputPassword
								height={20}
								label={"Enter your current password..."}
								value={values.currentPassword}
								onChange={handleChange("currentPassword")}
								onBlur={handleBlur('currentPassword')}
								error={errors.currentPassword}
								touched={touched.currentPassword}
							/>
							<InputWithLogo
								logo={"user"}
								height={20}
								label={"Enter your username..."}
								value={values.username}
								onChange={handleChange("username")}
								onBlur={handleBlur('username')}
								error={errors.username}
								touched={touched.username}
							/>
							<InputWithLogo
								logo={"mail"}
								height={20}
								label={"Enter your email address..."}
								value={values.email}
								onChange={handleChange("email")}
								onBlur={handleBlur('email')}
								error={errors.email}
								touched={touched.email}
							/>
							<View style={styles.buttonContainer}>
								<TouchableOpacity style={styles.button} onPress={() => handleSubmit()}>
									<Text style={styles.buttonText}>Save changes</Text>
								</TouchableOpacity>
							</View>
						</View>
					)}
				</Formik>
			</View>
		</SafeAreaView >
	);
}
