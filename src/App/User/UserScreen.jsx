import React, { useEffect, useContext } from 'react';
import { StyleSheet, SafeAreaView, View, Text, TouchableOpacity, Image, ScrollView, StatusBar } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import _collection from '../../../mocks/collection.json';

import UserContext from '../../components/context/UserContext';

import { getAuth, updateProfile } from "firebase/auth";

import { COLORS, SIZES } from '../../../constants/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: COLORS.black,
	},
	view_header: {
		flex: 2,
		width: "100%",
		flexDirection: "row",
		alignItems: "flex-start",
		justifyContent: "center",
	},
	username: {
		fontSize: 24,
		color: COLORS.white,
		fontWeight: "bold",
	},
	description: {
		color: "#535151",
		fontSize: 14,
		lineHeight: 14,
		fontWeight: "500",
	},
	view_RepoLowerPart: {
		paddingHorizontal: SIZES.padding,
		backgroundColor: COLORS.gray,
		flex: 3,
		borderRadius: 15,
		width: "100%",
		marginTop: 20,
	},
	view_RepoTabs: {
		flex: 2,
		backgroundColor: "#FFFFFF",
		justifyContent: "center",
		flexDirection: "row",
	},
	icon: {
		flexDirection: "row",
		borderRadius: 25,
		alignItems: "center",
		justifyContent: "center",
		height: 50,
		width: 50,
		marginRight: 5,
		marginTop: 20,
	},
	button: {
		padding: 10,
		width: "60%",
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: "#f53b3b",
		borderRadius: 5,
		marginTop: 20
	},
	end_view: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "flex-end",
		paddingRight: 20,
	},
});

export default function UserScreen({ navigation }) {

	const userContext = useContext(UserContext);
	const auth = getAuth();

	useEffect(() => {
		(async () => {
			if (Platform.OS !== 'web') {
				const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
				if (status !== 'granted') {
					alert('Sorry, we need camera roll permissions to make this work!');
				}
			}
		})();
	}, []);

	const pickImage = async () => {
		let result = await ImagePicker.launchImageLibraryAsync({
			mediaTypes: ImagePicker.MediaTypeOptions.All,
			allowsEditing: true,
			aspect: [4, 3],
			quality: 1,
		});

		if (!result.cancelled) {
			userContext.changePhotoURL(result.uri);
			updateProfile(auth.currentUser, {
				photoURL: result.uri
			}).then(() => {
				console.log('Profile updated successfully');
			}).catch((error) => {
				const errorCode = error.code;
				const errorMessage = error.message;

				console.error(errorCode, errorMessage);
			})
		}
	};

	return (
		<SafeAreaView style={styles.container}>
			{
				Platform.OS === "android" ?
					<StatusBar
						translucent
						backgroundColor="transparent"
					/>
					: <></>
			}
			<View style={styles.view_header}>
				<View style={styles.end_view}>
					<TouchableOpacity
						style={styles.icon}
						onPress={() => { navigation.navigate("EditScreen") }}
					>
						<MaterialCommunityIcons name="account-edit" size={40} color={COLORS.primary} />
					</TouchableOpacity>
					<TouchableOpacity
						style={styles.icon}
						onPress={() => { navigation.navigate("ChangePasswordScreen") }}
					>
						<MaterialCommunityIcons name="account-cog" size={40} color={COLORS.primary} />
					</TouchableOpacity>
					<TouchableOpacity
						style={styles.icon}
						onPress={() => {
							auth.signOut();
							userContext.changeIsLoggedStatus()
						}}
					>
						<MaterialCommunityIcons name="account-arrow-left" size={40} color={COLORS.primary} />
					</TouchableOpacity>
				</View>
			</View>
			<TouchableOpacity onPress={pickImage}>
				<Image
					source={{ uri: userContext.photoURL }}
					style={{ height: 100, width: 100, borderRadius: 50 }}
				/>
			</TouchableOpacity>
			<Text style={[styles.username, { marginTop: 15, marginBottom: 10 }]}>
				{userContext.username}
			</Text>
			<Text style={styles.description}>{userContext.email}</Text>
			<View style={styles.view_RepoLowerPart}>
				<ScrollView
					style={{
						marginBottom: 60
					}}>
				</ScrollView>
			</View>
		</SafeAreaView>
	);
}