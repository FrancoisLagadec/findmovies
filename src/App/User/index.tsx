import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { Routes } from "../../components/Navigation";

import UserScreen from "./UserScreen";
import EditScreen from "./EditScreen";
import ChangePasswordScreen from "./ChangePasswordScreen";

const UserStack = createStackNavigator<Routes>();
export const UserNavigator = () => {
	return (
		<UserStack.Navigator screenOptions={{ headerShown: false }}>
			<UserStack.Screen name="UserScreen" component={UserScreen} />
			<UserStack.Screen name="EditScreen" component={EditScreen} />
			<UserStack.Screen name="ChangePasswordScreen" component={ChangePasswordScreen} />
		</UserStack.Navigator>
	);
};