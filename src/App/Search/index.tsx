import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { Routes } from "../../components/Navigation";

import SearchScreen from "./SearchScreen";

const SearchStack = createStackNavigator<Routes>();
export const SearchNavigator = () => {
	return (
		<SearchStack.Navigator screenOptions={{ headerShown: false }}>
			<SearchStack.Screen name="SearchScreen" component={SearchScreen} />
		</SearchStack.Navigator>
	);
};