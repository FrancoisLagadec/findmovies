import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, FlatList, TouchableOpacity, SafeAreaView, StatusBar } from 'react-native';
import { Feather } from '@expo/vector-icons';

import InputWithLogo from '../../components/Form/InputWithLogo';

import _search from '../../../mocks/search.json';
import { COLORS, SIZES } from '../../../constants/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: COLORS.black,
	},
	searchSection: {
		marginTop: SIZES.padding + 6,
		marginLeft: '5%',
		marginRight: '5%',
		flexDirection: 'row'
	},
	searchButton: {
		paddingHorizontal: 10,
		marginVertical: (8),
		marginLeft: "5%",
		height: 62,
		backgroundColor: COLORS.primary,
		borderRadius: 15,
		alignItems: 'center',
		justifyContent: "center"
	},
	listItem: {
		backgroundColor: 'pink',
		justifyContent: 'center',
		alignItems: 'center'
	},
	listItemText: {
		fontSize: 14,
		fontWeight: 'bold'
	},
	errMsg: {
		marginTop: 20,
		textAlign: 'center',
		fontSize: 20,
		fontWeight: 'bold',
		color: 'red'
	}
});

export default function SearchScreen({ navigation }) {
	const [search, setSearch] = useState(null);
	const [movies, setMovies] = useState([]);
	const [requestState, setRequestState] = useState(false);

	const APIKey = "6f84ec42acmshc5e7e7ad58156e7p1dda4fjsn1dd506edb057"

	function onSearch() {
		if (search === null)
			return;
		console.log("New Request - Search film")
		fetch(`https://movies-tvshows-data-imdb.p.rapidapi.com/?type=get-movies-by-title&title=${search}`, {
			"method": "GET",
			"headers": {
				"x-rapidapi-host": "movies-tvshows-data-imdb.p.rapidapi.com",
				"x-rapidapi-key": APIKey
			}
		})
			.then(response => response.json())
			.then(data => {
				console.log(data)
				setMovies(data.movie_results)
			})
			.catch(err => {
				console.error(err);
			});
		setRequestState(true)
	};

	function getImage(imdb_id) {
		return new Promise((resolve, reject) => {
			console.log("New Request - Get Image")
			fetch(`https://movies-tvshows-data-imdb.p.rapidapi.com/?type=get-movies-images-by-imdb&imdb=${imdb_id}`, {
				"method": "GET",
				"headers": {
					"x-rapidapi-host": "movies-tvshows-data-imdb.p.rapidapi.com",
					"x-rapidapi-key": APIKey
				}
			})
				.then(response => resolve(response.json()))
		})
	};

	function renderList() {
		return (
			<FlatList
				vertical
				showsVerticalScrollIndicator={false}
				contentContainerStyle={{
					marginTop: SIZES.padding
				}}
				data={movies}
				keyExtractor={item => `${item.imdb_id}`}
				renderItem={({ item, index }) => {
					return (
						<TouchableWithoutFeedback
							onPress={() => {
								getImage(item.imdb_id).then((result) => {
									item.fanart = result.fanart
									item.poster = result.poster
									navigation.navigate('MovieDetails', { selectedMovie: item })
								})
							}
							}
							key={index}
						>
							<View
								style={{
									alignItems: 'center',
									flexDirection: 'row',
									justifyContent: 'space-between',
									marginHorizontal: 20,
									marginVertical: 5,
									padding: 10,
									backgroundColor: COLORS.transparentWhite

								}}
							>
								{/* NAME */}
								<Text
									style={{
										maxWidth: "90%",
										marginTop: SIZES.base,
										color: COLORS.white,
										fontSize: SIZES.h3
									}}
								>
									{item.title}
								</Text>
								<Feather
									name={"chevron-right"}
									size={20}
									color={COLORS.primary}
								/>
							</View>
						</TouchableWithoutFeedback>
					)
				}}
			/>
		)
	}

	return (
		<SafeAreaView style={[styles.container, { paddingBottom: 60 }]}>
			{
				Platform.OS === "android" ?
					<StatusBar
						translucent
						backgroundColor="transparent"
					/>
					: <></>
			}
			<View style={styles.searchSection}>
				<View
					style={{
						maxWidth: '75%'
					}}
				>
					<InputWithLogo
						logo={"search"}
						height={20}
						label={"Search a movie..."}
						value={search}
						onChange={setSearch}
					/>
				</View>
				<TouchableOpacity
					style={styles.searchButton}
					onPress={onSearch}
				>
					<Text style={{ color: COLORS.white, fontWeight: 'bold', fontSize: 18 }}>Search</Text>
				</TouchableOpacity>
			</View>

			{!requestState
				?
				<View
					style={{
						height: SIZES.width,
						alignItems: 'center',
						justifyContent: 'center'
					}}>
					<Text style={{ color: COLORS.mediumGray }}>Start a research.</Text>
				</View>
				:
				(movies && movies.length > 0)
					?
					renderList()
					:
					<View
						style={{
							height: SIZES.width,
							alignItems: 'center',
							justifyContent: 'center'
						}}>
						<Text style={{ color: COLORS.mediumGray }}>We didn't find this movie.</Text>
					</View>
			}
		</SafeAreaView >
	);
}
