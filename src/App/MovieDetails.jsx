import React, { useState, useEffect } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, ScrollView, StyleSheet, Platform, ActivityIndicator, StatusBar, SafeAreaView, Modal, Pressable, TextInput } from 'react-native';
import { Feather, Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import cheerio from "react-native-cheerio";

import { getAuth } from "firebase/auth";
import { getDatabase, ref, child, get, set } from "firebase/database";

import { GetLists, isMovieInList } from '../components/GetData';

import { COLORS, SIZES } from '../../constants/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: COLORS.black,
	},
	subSectionContainer: {
		paddingTop: 15,
		marginBottom: 90
	},
	categoryContainer: {
		flexDirection: 'row',
		backgroundColor: COLORS.gray1,
		borderRadius: SIZES.base,
		marginLeft: SIZES.base,
		paddingHorizontal: SIZES.base,
		paddingVertical: 3,
		justifyContent: 'center',
		alignItems: 'center'
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		marginTop: 22,
	},
	modalView: {
		margin: 20,
		width: '70%',
		backgroundColor: "white",
		borderRadius: 20,
		padding: 35,
		alignItems: "center",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		elevation: 5
	},
	textStyle: {
		color: "white",
		fontWeight: "bold",
		textAlign: "center"
	},
	modalText: {
		marginBottom: 15,
		textAlign: "center",
		fontSize: SIZES.h2,
		fontWeight: 'bold',
		textDecorationLine: 'underline',
	},
	button: {
		borderRadius: 10,
		padding: 10,
		elevation: 2,
		marginHorizontal: 5,
		marginTop: 20,
	},
	buttonAddList: {
		backgroundColor: COLORS.primary,
	},
	buttonClose: {
		backgroundColor: COLORS.gray,
	},
	itemSelected: {
		backgroundColor: COLORS.primary,
		marginBottom: 10,
		borderRadius: 5,
		overflow: 'hidden',
		width: '85%',
	},
	items: {
		backgroundColor: COLORS.gray1,
		marginBottom: 10,
		borderRadius: 5,
		overflow: 'hidden',
		width: '85%',
	}
});

export default function MovieDetails({ navigation, route }) {
	const [selectedMovie, setSelectedMovie] = useState(null);
	const [informationsMovie, setInformationsMovie] = useState(null);
	const [modalVisible, setModalVisible] = useState(false);
	const [refList, setRefList] = useState([]);
	const [selectedItems, setSelectedItems] = useState(-1);
	const [newInput, setNewInput] = useState(false);
	const [inputField, setInputField] = useState('');
	const [wish, setWish] = useState(false);
	const [existInList, setExistInList] = useState([]);

	const auth = getAuth();
	const db = getDatabase();
	const dbRef = ref(getDatabase());

	useEffect(() => {
		let { selectedMovie } = route.params;
		setSelectedMovie(selectedMovie)
		ScrapCategories(route.params?.selectedMovie?.imdb_id)

		async function getData() {
			await GetLists(dbRef, auth.currentUser.uid).then((listsName) => {
				setRefList(listsName)
				if (listsName.length > 0) {
					let array = []

					listsName.forEach((name) => {
						isMovieInList(dbRef, auth.currentUser.uid, selectedMovie.imdb_id, name)
							.then((result) => {
								if (result) {
									array.push(true)
								}
								else
									array.push(false)
							})
					})
					setExistInList(array)
				}
			})

			let tmp = [];

			await get(child(dbRef, `wishes/${auth.currentUser.uid}/${selectedMovie.imdb_id}`)).then((user) => {
				if (user.exists())
					setWish(true)
			}).catch((error) => {
				console.error(error);
			});
			return tmp;

		}
		getData()
	}, [])

	async function ScrapCategories(id) {
		const searchUrl = `https://pro.imdb.com/title/${id}`;
		const response = await fetch(searchUrl);

		const htmlString = await response.text();
		const $ = cheerio.load(htmlString);
		var categories = $("span#genres").text()
		var runningTime = $("span#running_time").text().trim()
		var directorSummary = $("div#director_summary div").last().text().replace(/\n/g, '')
		var writerSummary = $("div#writer_summary div").last().text().replace(/\n/g, '')
		var titleSummary = await $("div#title_summary").text()

		const infos = {
			categories: categories.split(','),
			runningTime: runningTime.substring(0, runningTime.indexOf(' min')),
			titleSummary: titleSummary.search("Read more:") === -1 ? titleSummary : titleSummary.substring(0, titleSummary.indexOf('Read more:')),
			directorSummary: directorSummary.trim(),
			writerSummary: writerSummary.trim()
		}
		setInformationsMovie(infos)
		return infos;
	}

	function renderHeaderBar() {
		return (
			<View
				style={{
					flexDirection: 'row',
					justifyContent: 'space-between',
					alignItems: 'center',
					marginTop: Platform.OS === 'ios' ? 50 : 30,
					paddingHorizontal: SIZES.padding
				}}
			>
				{/* Go Back */}
				<TouchableOpacity
					style={{
						alignItems: 'center',
						justifyContent: 'center',
						width: 40,
						height: 40,
						borderRadius: 15,
						backgroundColor: COLORS.transparentBlack
					}}
					onPress={() => navigation.goBack()}
				>
					<Feather
						name={"chevron-left"}
						size={20}
						color={COLORS.white}
					/>
				</TouchableOpacity>

				{/* Wish */}
				<TouchableOpacity
					style={{
						alignItems: 'center',
						justifyContent: 'center',
						width: 40,
						height: 40,
						borderRadius: 15,
						backgroundColor: COLORS.transparentBlack
					}}
					onPress={() => {
						if (!wish) {
							set(ref(db, 'wishes/' + auth.currentUser.uid + '/' + selectedMovie.imdb_id), {
								fanart: selectedMovie.fanart,
								imdb_id: selectedMovie.imdb_id,
								poster: selectedMovie.poster,
								title: selectedMovie.title,
								year: selectedMovie.year
							})
						} else {
							set(ref(db, 'wishes/' + auth.currentUser.uid + '/' + selectedMovie.imdb_id), {
								fanart: null,
								imdb_id: null,
								poster: null,
								title: null,
								year: null
							})
						}

						setWish(!wish);
					}}
				>
					<Ionicons
						style={wish ? { color: COLORS.primary, } : {}}
						name={wish ? "bookmark" : "bookmark-outline"}
						size={20}
						color={COLORS.white}
					/>
				</TouchableOpacity>
			</View>
		)
	}

	function renderHeader() {
		return (
			<ImageBackground
				source={{ uri: selectedMovie?.fanart }}
				resizeMode="cover"
				style={{
					width: "100%",
					height: SIZES.height < 700 ? SIZES.height * 0.6 : SIZES.height * 0.7
				}}
			>
				<View
					style={{
						flex: 1
					}}
				>
					{renderHeaderBar()}

					<View
						style={{
							flex: 1,
							justifyContent: 'flex-end'
						}}
					>
						<LinearGradient
							locations={[0.6, 0.6, 0.6, 0.95]}
							colors={[
								'rgba(0, 0, 0, 0)',
								'rgba(0, 0, 0, 0)',
								'rgba(0, 0, 0, 0)',
								'rgba(0, 0, 0, 1)',
							]}
							style={{
								height: "100%",
								alignItems: 'center',
								justifyContent: 'flex-end'
							}}
						>
							{/* Title */}
							<Text
								style={{
									marginTop: SIZES.base,
									color: COLORS.white,
									fontSize: SIZES.h1
								}}
							>
								{selectedMovie?.title}
							</Text>
						</LinearGradient>
					</View>
				</View>
			</ImageBackground>
		)
	}

	function renderCategoryAndRunningTime() {
		return (
			<View
				style={{
					flexDirection: 'row',
					justifyContent: 'center',
					alignItems: 'center',
					marginTop: SIZES.base,
				}}
			>
				{/* Genre */}
				<View
					style={[
						styles.categoryContainer,
						{
							marginLeft: 0
						}
					]}
				>
					<Text
						style={{
							color: COLORS.white,
							fontSize: SIZES.h4
						}}
					>
						{informationsMovie?.categories[0]}
					</Text>
				</View>

				{/* RunningTime */}
				<View
					style={styles.categoryContainer}
				>
					<Text
						style={{
							color: COLORS.white,
							fontSize: SIZES.h4
						}}
					>
						{informationsMovie?.runningTime} min
					</Text>
				</View>
			</View>
		)
	}

	function renderDescription() {
		return (
			<View
				style={{
					marginTop: SIZES.base,
					paddingHorizontal: SIZES.padding
				}}
			>
				{!informationsMovie ?
					<ActivityIndicator
						size='large'
						color={COLORS.primary}
					/>
					:
					<View>

						{/* SYNOPSIS */}
						<Text
							style={{
								textAlign: 'justify',
								color: COLORS.white,
								fontSize: SIZES.h3
							}}
						>
							{informationsMovie?.titleSummary}
						</Text>

						{/* Director */}
						<View
							style={{
								paddingTop: 5,
								flexDirection: 'row',
								paddingBottom: 5,
								paddingRight: 90
							}}>
							<Text
								style={{
									width: 90,
									fontSize: SIZES.h4,
									color: COLORS.white,
								}}>
								Director
							</Text>
							<Text
								style={{
									textAlign: 'justify',
									color: COLORS.white,
									fontSize: SIZES.h4,
								}}
							>
								{informationsMovie?.directorSummary}
							</Text>
						</View>
						{/* Writer */}
						<View
							style={{
								flexDirection: 'row',
								paddingBottom: 30,
								paddingRight: 90
							}}>
							<Text
								style={{
									width: 90,
									fontSize: SIZES.h4,
									color: COLORS.white,
								}}>
								Writer
							</Text>
							<Text
								style={{
									textAlign: 'justify',
									color: COLORS.white,
									fontSize: SIZES.h4,
								}}
							>
								{informationsMovie?.writerSummary}
							</Text>
						</View>
						<TouchableOpacity
							style={{
								padding: 10,
								backgroundColor: COLORS.primary,
								alignItems: 'center',
								width: '60%',
								margin: 'auto',
								alignSelf: 'center',
								marginBottom: 20
							}}

							onPress={() => setModalVisible(true)}
						>
							<Text style={{ color: COLORS.white, fontSize: SIZES.h2 }}>Add to a list</Text>
						</TouchableOpacity>
					</View>
				}
			</View>
		)
	}

	const RenderModalContent = ({ item, index }) => (
		<TouchableOpacity
			style={(selectedItems === index || existInList[index] === true) ? styles.itemSelected : styles.items}
			key={index}
			onPress={() => { setSelectedItems(index) }}
			disabled={existInList[index]}
		>
			<View style={{ padding: 8 }}>
				<Text
					style={{
						color: COLORS.white,
						fontSize: SIZES.h3
					}}
				>{item}</Text>
			</View>
		</TouchableOpacity>
	)

	return (
		<SafeAreaView style={styles.container}>
			{
				Platform.OS === "android" ?
					<StatusBar
						translucent
						backgroundColor="transparent"
					/>
					: <></>
			}
			<Modal
				animationType="slide"
				transparent={true}
				visible={modalVisible}
				onRequestClose={() => {
					setModalVisible(!modalVisible);
				}}
			>
				<View style={styles.centeredView}>
					<View style={styles.modalView}>
						<Text style={styles.modalText}>Choose a list</Text>


						{refList && refList.map((item, index) => {
							return (
								<RenderModalContent item={item} index={index} />
							)
						})
						}

						{refList && refList.length ? (
							null
						) :
							<Text style={{ paddingTop: 10 }}>You have not yet created a list.</Text>
						}

						{newInput ?
							<View
								style={{
									flexDirection: 'row',
									backgroundColor: COLORS.gray,
									width: '85%',
									marginBottom: 10,
									borderRadius: 5,
									overflow: 'hidden',
								}}>
								<TextInput
									onChangeText={text => { setInputField(text) }}
									placeholder={"Enter a name..."}
									style={{
										marginLeft: 10,
										marginRight: 25,
										color: COLORS.white,
									}}
								/>
								<TouchableOpacity
									style={{
										right: 0,
										position: 'absolute',
										alignContent: 'center',
										alignSelf: 'center',
									}}
									onPress={() => {
										if (inputField !== '') {
											setRefList([...refList, inputField])
											setInputField('')
										}
										setNewInput(!newInput)
									}
									}
								>
									<Feather
										style={{
										}}
										name={'plus'}
										size={23}
										color={COLORS.white}
									/>
								</TouchableOpacity>
							</View>
							:
							<TouchableOpacity
								style={{
									padding: 7
								}}
								onPress={() => setNewInput(!newInput)}
							>
								<Feather
									name={'plus'}
									size={30}
									color={COLORS.primary}
								/>
							</TouchableOpacity>
						}
						<View style={{ flexDirection: 'row' }}>
							{refList && refList.length ? (
								<Pressable
									style={[styles.button, styles.buttonAddList]}
									onPress={() => {
										if (selectedItems !== -1) {
											set(ref(db, 'lists/' + auth.currentUser.uid + '/' + refList[selectedItems] + '/' + selectedMovie.imdb_id), {
												fanart: selectedMovie.fanart,
												imdb_id: selectedMovie.imdb_id,
												poster: selectedMovie.poster,
												title: selectedMovie.title,
												year: selectedMovie.year
											})
											setNewInput(false)
											setModalVisible(!modalVisible);
										}
									}}
								>
									<Text style={styles.textStyle}>Add</Text>
								</Pressable>
							) : null
							}
							<Pressable
								style={[styles.button, styles.buttonClose]}
								onPress={() => {
									setSelectedItems(-1);
									setNewInput(false)
									setModalVisible(!modalVisible);
								}}
							>
								<Text style={styles.textStyle}>Close</Text>
							</Pressable>
						</View>
					</View>
				</View>
			</Modal>

			<ScrollView
				contentContainerStyle={{ backgroundColor: COLORS.black }}
				style={{ flex: 1, backgroundColor: COLORS.black, marginBottom: 60 }}
			>
				{/* Header */}

				{renderHeader()}

				{/* Category */}

				{informationsMovie ? renderCategoryAndRunningTime() : <></>}

				{/* Description */}

				{renderDescription()}

			</ScrollView>
		</SafeAreaView >
	)
}
