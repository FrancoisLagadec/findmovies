import React, { useState } from "react";
import { FontAwesome5 } from '@expo/vector-icons';
import { StyleSheet, TextInput, View } from 'react-native';

const styles = StyleSheet.create({
	content: {
		flexDirection: 'row',
		width: '100%',
		backgroundColor: '#fff',
		textAlign: 'center',
	},
	searchIcon: {
		alignSelf: 'center',
		left: 15,
	},
	searchInput: {
		marginHorizontal: 20,
		padding: 10,
		borderRadius: 10,
		width: '70%',
		textDecorationLine: 'underline',
		fontSize: 20,
		backgroundColor: '#E5E8E8',
	}
})

function SearchBar(props) {
	const { onSearch } = props;
	const [searchText, setSearchText] = useState('')

	return (
		<View style={styles.content}>
			<FontAwesome5
				style={styles.searchIcon}
				name="search"
				size={18}
				color='gray'
			/>
			<TextInput
				type="text"
				placeholder="Search your movies"
				onChangeText={
					(value) => {
						setSearchText(value)
						onSearch(value)
					}
				}
				style={styles.searchInput}
				value={searchText}
			/>
		</View>
	);
}

export default SearchBar;
