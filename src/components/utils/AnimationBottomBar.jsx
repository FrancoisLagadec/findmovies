import React from 'react';
import { Dimensions, Animated } from 'react-native';
import { COLORS } from '../../../constants/theme';

export function getWidth() {
	let width = Dimensions.get('window').width

	return width / 4
}

export function AnimationBottomBar({ tabOffsetValue }) {
	return (
		<Animated.View style={{
			backgroundColor: COLORS.primary,
			position: 'absolute',
			bottom: 60,
			width: getWidth(),
			height: 2,
			transform: [
				{ translateX: tabOffsetValue }
			]
		}}>
		</Animated.View>
	);
}