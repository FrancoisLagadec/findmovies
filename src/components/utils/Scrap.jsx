import cheerio from "react-native-cheerio";

export async function ScrapPoster(id) {
	const searchUrl = `https://www.imdb.com/title/${id}/`;
	const response = await fetch(searchUrl);

	const htmlString = await response.text();
	const $ = cheerio.load(htmlString);
	var image = await $("img.ipc-image").attr("src")
	console.log(image)
	console.log("New image")

	return image;
}
