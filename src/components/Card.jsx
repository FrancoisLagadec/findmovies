import React from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		marginVertical: 5
	},
	imageContainer: {
		flex: 34,
	},
	image: {
		height: 140,
		width: "90%",
	},
	info: {
		flex: 66,
		padding: 15
	},
	title: {
		color: 'white',
		marginVertical: 5,
		fontWeight: 'bold',
		fontSize: 18,
	},
	year: {
		color: 'white',
		fontSize: 16,
	},
})

export default function Card(props) {
	const { navigation, movie } = props;

	return (
		<TouchableOpacity
			onPress={() => { navigation.navigate('MovieDetails', { selectedMovie: movie }) }}
			style={styles.container}>
			<View style={styles.imageContainer}>
				<Image
					source={{ uri: movie.poster }}
					style={styles.image}
				/>
			</View>
			<View style={styles.info}>
				<Text style={styles.title}>{movie.title}</Text>
				<Text style={styles.year}>{movie.year}</Text>
			</View>
		</TouchableOpacity>
	);
}
