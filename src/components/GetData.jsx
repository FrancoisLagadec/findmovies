import { child, get } from "firebase/database";

export async function GetLists(dbRef, user_id) {
	let tmp = [];

	await get(child(dbRef, `lists/${user_id}`)).then((user) => {
		if (user.exists()) {
			user.forEach((list) => {
				tmp.push(list.key);
			})
		} else {
			console.log("No data available");
		}
	}).catch((error) => {
		console.error(error);
	});
	return tmp;
}

export function isMovieInList(dbRef, user_id, imdb_id, listName) {
	return new Promise((resolve, reject) => {
		get(child(dbRef, `lists/${user_id}/${listName}`)).then((movie) => {
			movie.forEach((_movie) => {
				if (_movie.key === imdb_id) {
					resolve(true)
				}
			})
		})
	})
}

export function GetIndexOfList(dbRef, user_id, imdb_id, listsName) {
	return new Promise((resolve, reject) => {
		let list = []
		let index = 0

		console.log("\n\n\n", listsName)
		for (const list of listsName) {
			console.log("---A---\nAnalyse de la liste ", list)

			// Search Lists content
			get(child(dbRef, `lists/${user_id}/${list}`)).then((movie) => {
				console.log("---B---\nAnalyse du contenu de la liste", list)

				// // Check Movie's name
				// movie.forEach((_movie) => {
				//     console.log("---C---")
				//     console.log("[" + _movie.key + "]")
				//     console.log("[" + imdb_id + "]")
				//     if (_movie.key === imdb_id) {
				//         list.push(index)
				//         resolve(list)
				//     }
				// })
			})
			index++;
		}
	})
}

export function getWishes(dbRef, user_id) {
	return new Promise((resolve, reject) => {
		let tmp = []

		get(child(dbRef, `wishes/${user_id}/`)).then((data) => {
			data.forEach((movie) => {
				tmp.push(movie)
			})
			resolve(tmp)
		})
	})
}