import { createContext } from 'react';

const UserContext = createContext({
	islogged: false,
	changeIsLoggedStatus: () => { },
	username: "",
	changeUsername: (val) => { },
	email: "",
	changeEmail: (val) => { },
	photoURL: "",
	changePhotoURL: (val) => { },

});

export default UserContext;