import React, { useState } from 'react';
import { Text, TextInput, View, StyleSheet, TouchableHighlight } from 'react-native';
import { Feather } from '@expo/vector-icons';

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		lineHeight: 1.2,
		color: "#B3ADAC",
		backgroundColor: "white",
		width: '100%',
		height: 62,
		borderRadius: 13,
		paddingRight: 30,
		paddingLeft: 15,
		alignItems: "center"
	}
});

interface InputPasswordProps {
	height: number;
	label: string;
	value: string;
	onChange: any;
	onBlur: any;
	error?: string;
	touched?: boolean;
}

const InputPassword = ({ height, label, value, onChange, onBlur, error, touched }: InputPasswordProps) => {
	const [visible, setVisible] = useState(false);

	return (
		<View
			style={{
				display: 'flex',
				marginVertical: (8),
			}}>
			<View style={styles.container}>
				<Feather
					name="lock"
					size={height}
					color="#B3ADAC"
				/>
				<TextInput
					style={{
						fontSize: 18,
						paddingHorizontal: 15,
						color: "black",
						width: 280,
						maxWidth: '100%'
					}}
					onChangeText={text => { onChange(text) }}
					onBlur={onBlur}
					value={value}
					placeholder={label}
					secureTextEntry={!visible}
				/>
				<TouchableHighlight onPress={() => setVisible(!visible)}>
					<Feather
						name={visible === true ? "eye" : "eye-off"}
						size={height}
						color="#B3ADAC"
					/>
				</TouchableHighlight>
			</View>
			<Text style={{ color: '#FF0000', fontSize: 16 }}>{error}</Text>
		</View>
	);
};

export default InputPassword;
