import React from 'react';
import { Text, TextInput, View, StyleSheet } from 'react-native';
import { Feather } from '@expo/vector-icons';

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		lineHeight: 1.2,
		color: "#B3ADAC",
		backgroundColor: "white",
		width: '100%',
		height: 62,
		borderRadius: 13,
		paddingRight: 30,
		paddingLeft: 15,
		alignItems: "center"
	}
});

interface InputWithLogoProps {
	logo: string;
	height: number;
	label: string;
	value: string;
	onChange: any;
	onBlur: any;
	error?: string;
	touched?: boolean;
}

const InputWithLogo = ({ logo, height, label, value, onChange, onBlur, error, touched }: InputWithLogoProps) => {
	const reColor = !touched ? "white" : (error ? "red" : "green");

	return (
		<View
			style={{
				display: 'flex',
				marginVertical: (8),
			}}>
			<View style={styles.container}>
				<Feather
					name={logo}
					size={height}
					color="#B3ADAC"
				/>
				<TextInput
					style={{
						fontSize: 18,
						paddingHorizontal: 15,
						width: 280,
						maxWidth: '100%',
					}}
					onChangeText={text => { onChange(text) }}
					onBlur={onBlur}
					value={value}
					placeholder={label}
				/>
			</View>
			<Text style={{ color: '#FF0000', fontSize: 16 }}>{error}</Text>
		</View>
	);
};

export default InputWithLogo;
