import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { Feather } from '@expo/vector-icons';

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
	},
	checkbox: {
		marginRight: 5,
		borderRadius: 4,
		justifyContent: 'center',
		alignItems: "center",
		borderWidth: 1,
		borderColor: 'white',
	}
});

interface CheckBoxProps {
	label: string;
	size: number;
	checked: boolean;
	onChange: any;
}

const CheckBox = ({ label, size, checked, onChange }: CheckBoxProps) => {
	return (
		<TouchableOpacity onPress={() => onChange()}>
			<View style={styles.container}>
				<View style={[{ height: size, width: size, backgroundColor: checked ? "white" : "black" }, styles.checkbox]}>
					<Feather
						name="check"
						size={size}
						color="black"
					/>
				</View>
				<Text style={{ color: "white", fontSize: size - 6 }}>{label}</Text>
			</View>
		</TouchableOpacity>
	);
};

export default CheckBox;
