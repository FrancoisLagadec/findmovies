import React from "react";
import { View } from 'react-native';
import Card from "./Card";

function CardList({ navigation, results }) {
	let data = [];
	if (results) {
		data = results;
	}
	return (
		<View>
			{data.map((item, index) => {
				/* return strange object w/ part of it who are string */
				var str = JSON.stringify(item)
				var obj = JSON.parse(str)

				return (<Card navigation={navigation} key={index} movie={obj} />)
			})}
		</View>
	);
}

export default CardList;